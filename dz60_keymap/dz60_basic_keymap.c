#include QMK_KEYBOARD_H

#define MODS_CTRL_MASK  (MOD_BIT(KC_LSHIFT)|MOD_BIT(KC_RSHIFT))

#define _DEFAULT 0
#define _ALTFN 1

#define ______ KC_TRNS

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

  /* Base Layer
   * ,-----------------------------------------------------------------------------------------.
   * | ESC |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  |  0  |  -  |  =  |   Bkspc   |
   * |-----------------------------------------------------------------------------------------+
   * | Tab    |  Q  |  W  |  E  |  R  |  T  |  Y  |  U  |  I  |  O  |  P  |  [  |  ]  |    \   |
   * |-----------------------------------------------------------------------------------------+
   * | LAY1    |  A  |  S  |  D  |  F  |  G  |  H  |  J  |  K  |  L  |  ;  |  '  |    Enter    |
   * |-----------------------------------------------------------------------------------------+
   * | Shift     |  Z  |  X  |  C  |  V  |  B  |  N  |  M  |  ,  |  .  |  /  | RSh             |
   * |-----------------------------------------------------------------------------------------+
   * | Ctrl |  Cmd  |  Alt  |                         SPACE     | RAlt |  L  |  D  |  U  |   R |
   * `-----------------------------------------------------------------------------------------'
   */
	[_DEFAULT] = LAYOUT(
		KC_ESCAPE, KC_1, KC_2, KC_3, KC_4, KC_5, KC_6, KC_7, KC_8, KC_9, KC_0, KC_MINS, KC_EQL, KC_NO, KC_BSPC,
		KC_TAB, KC_Q, KC_W, KC_E, KC_R, KC_T, KC_Y, KC_U, KC_I, KC_O, KC_P, KC_LBRC, KC_RBRC, KC_BSLS,
		MO(_ALTFN), KC_A, KC_S, KC_D, KC_F, KC_G, KC_H, KC_J, KC_K, KC_L, KC_SCLN, KC_QUOT, KC_ENT,
		KC_LSFT, KC_NO, KC_Z, KC_X, KC_C, KC_V, KC_B, KC_N, KC_M, KC_COMM, KC_DOT, KC_SLSH, KC_RSFT, KC_NO,
		KC_LCTL, KC_LALT, KC_LGUI, KC_SPC, KC_SPC, KC_SPC, KC_RALT, KC_LEFT, KC_DOWN, KC_UP, KC_RIGHT),

  /* 
   * ,-----------------------------------------------------------------------------------------.
   * | `   |  F1 |  F2 |  F3 |  F4 |  F5 |  F6 |  F7 |  F8 |  F9 | F10 | F11 | F12 |   DEL     |
   * |-----------------------------------------------------------------------------------------+
   * |       | Lclk | MsUp | Rclk |     |     |     |     |     |      |     |     |    |      |
   * |-----------------------------------------------------------------------------------------+
   * |        |MsLft|MsDwn|MsRght|   |   |     |     |      |      |       |     |     |       |
   * |-----------------------------------------------------------------------------------------+
   * |           |     |     |     |     |PREV|NEXT |PLAY |VolDwn|VolUp| Mute|                 |
   * |-----------------------------------------------------------------------------------------+
   * |      |       |       |                                   |     | HOME | PgUp|PgDn| END  |
   * `-----------------------------------------------------------------------------------------'
   */
	[_ALTFN] = LAYOUT(
		KC_GRV, KC_F1, KC_F2, KC_F3, KC_F4, KC_F5, KC_F6, KC_F7, KC_F8, KC_F9, KC_F10, KC_F11, KC_F12, ______, KC_DEL,
		______, KC_BTN1, KC_MS_U, KC_BTN2, RGB_TOG, RGB_MOD, RGB_HUI, RGB_HUD, RGB_SAI, RGB_SAD, RGB_VAI, RGB_VAD,  ______, RESET,
		______, KC_MS_L, KC_MS_D, KC_MS_R, ______, ______, ______, BL_DEC, BL_TOGG, BL_INC, BL_STEP,  ______, ______,
		______, ______, ______, ______, ______, ______, KC_MPRV, KC_MNXT, KC_MPLY, KC_VOLD, KC_VOLU, KC_MUTE, ______, ______,
		______, ______, ______, ______, ______, ______, ______, KC_HOME, KC_PGUP, KC_PGDOWN, KC_END),
};

