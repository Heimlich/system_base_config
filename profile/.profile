# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"
[[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm" # Load RVM into a shell session *as a function*

export PASSWORD_STORE_DIR=/home/hans/Documents/kollektiv/git_repositories/pass_db_coderat

export PATH="$HOME/.poetry/bin:$PATH"
