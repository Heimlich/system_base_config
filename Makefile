.PHONY: all dotfiles

all: dotfiles

dotfiles:
	for folder in $(shell find . -maxdepth 1 -type d -not -name ".git" -not -name "." -not -name "*etc" -not -name "*wiki"); do \
		f=$$(echo $$folder | sed -e 's,./,,'); \
		stow -R $$f; \
	done
